# EC2

resource "aws_instance" "master" {
  count           = 9
  ami             = var.aws_ec2_ami
  subnet_id       = aws_subnet.sbnt_public1.id
  instance_type   = var.aws_instance_type
  security_groups = [aws_security_group.sg_public.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  user_data       = file("postinstall-m.sh")

  tags = {
    Name = "master-${count.index + 1}"
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> public_ips.txt"
  }
}
